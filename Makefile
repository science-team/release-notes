#!/usr/bin/make -f

text=wheezy

DISTDIR=wheezy

MARKDOWN = pandoc -f markdown

all: html

html:
	${MARKDOWN} -t html $(text).md > $(text).html

clean:
	rm -f $(text).html
