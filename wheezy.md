# Release notes for Wheezy from Debian Science Team #

During the preparation period Debian Science Team tried to
update as many as possible packages to newer versions, which
are already inside the archive, as well as to bring new packages
for different fields of science.


The metapackages serve to install a useful set of packages 
for someone working in a particular scientific field. Since
Squeeze-release have been added 3 new sets of packages:

* [**Simulations**](http://blends.alioth.debian.org/science/tasks/simulations): 
  contains the packages that are used to do simulations 
  in different fields of science.
* [**High energy physics (devel)**](http://blends.alioth.debian.org/science/tasks/highenergy-physics-dev): 
  installs Debian Science packages related to 
  development of High Energy Physics applications, which is a branch of physics 
  that studies the elementary subatomic constituents of matter and radiation, 
  and their interactions.
* [**Physics-dev**](http://blends.alioth.debian.org/science/tasks/physics-dev): 
  packages which are helpful for development of applications for 
  Physics and Mathematics.
